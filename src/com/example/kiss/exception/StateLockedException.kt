package com.example.kiss.exception

data class StateLockedException(
    val text: String
): RuntimeException(text)