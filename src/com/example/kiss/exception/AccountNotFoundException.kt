package com.example.kiss.exception

data class AccountNotFoundException(
    val text: String
): RuntimeException(text)