package com.example.kiss.exception

data class NotEnoughBalanceException (
    val text: String
): RuntimeException(text)