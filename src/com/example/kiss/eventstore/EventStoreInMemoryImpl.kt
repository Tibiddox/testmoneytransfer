package com.example.kiss.eventstore

import com.example.kiss.eventstore.entity.EventEntity
import java.util.concurrent.ConcurrentHashMap


/**
 * It is C in CQRS
 * Command storage
 * */
class EventStoreInMemoryImpl: EventStore{

    val storage: ConcurrentHashMap<String, MutableList<EventEntity>> = ConcurrentHashMap()

    override fun getEventsForAggregate(id: String): List<EventEntity> {
        return storage[id] ?: emptyList()
    }

    override fun addNewEventForAggregate(id: String, event: EventEntity) {
        if (storage.containsKey(id)){
            storage[id]!!.add(event)
        } else {
            storage[id] = mutableListOf(event)
        }
    }
}