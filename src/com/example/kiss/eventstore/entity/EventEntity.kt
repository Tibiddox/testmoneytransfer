package com.example.kiss.eventstore.entity

import com.example.kiss.domain.Event
import java.time.Instant
import java.util.*

data class EventEntity (
    val id: UUID = UUID.randomUUID(),
    val aggregateName: String,
    val aggregateId: String,
    val event: Event,
    val time: Instant = Instant.now()
)