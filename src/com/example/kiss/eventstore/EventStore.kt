package com.example.kiss.eventstore

import com.example.kiss.eventstore.entity.EventEntity

interface EventStore {

    fun getEventsForAggregate(id: String): List<EventEntity>

    fun addNewEventForAggregate(id: String, event: EventEntity)
}