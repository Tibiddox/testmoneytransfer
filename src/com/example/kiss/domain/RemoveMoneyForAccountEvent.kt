package com.example.kiss.domain

import java.time.Instant

data class RemoveMoneyForAccountCommand(
    val id: String,
    val amount: Double,
    val time: Instant = Instant.now()
): Command

data class RemoveMoneyForAccountEvent(
    val id: String,
    val amount: Double,
    val time: Instant = Instant.now()
): Event