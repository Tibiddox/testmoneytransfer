package com.example.kiss.domain

import java.time.Instant

data class AddMoneyForAccountCommand(
    val id: String,
    val amount: Double,
    val time: Instant = Instant.now()
): Command

data class AddMoneyForAccountEvent(
    val id: String,
    val amount: Double,
    val time: Instant = Instant.now()
): Event