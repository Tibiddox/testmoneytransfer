package com.example.kiss.domain.transfer

import com.example.kiss.domain.Command
import com.example.kiss.domain.Event
import java.time.Instant
import java.util.*

data class TransferMoneyForAnotherAccountCommand(
    val transferId: UUID = UUID.randomUUID(),
    val id: String,
    val amount: Double,
    val toId: String,
    val time: Instant = Instant.now()
): Command

data class TransferMoneyForAnotherAccountEvent(
    val transferId: UUID = UUID.randomUUID(),
    val id: String,
    val amount: Double,
    val toId: String,
    val time: Instant = Instant.now()
): Event