package com.example.kiss.domain.transfer

import com.example.kiss.domain.Command
import com.example.kiss.domain.Event
import java.util.*

data class FinishTransferMoneyToAnotherAccountCommand(
    val transferId: UUID,
    val id: String,
    val amount: Double,
    val fromId: String
): Command

data class FinishTransferMoneyToAnotherAccountEvent(
    val transferId: UUID,
    val amount: Double,
    val id: String,
    val fromId: String
): Event