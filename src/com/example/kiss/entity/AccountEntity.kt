package com.example.kiss.entity

data class AccountEntity (
    var id: String,
    var balance: Double
)