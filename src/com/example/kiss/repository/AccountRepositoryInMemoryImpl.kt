package com.example.kiss.repository

import com.example.kiss.entity.AccountEntity
import com.example.kiss.exception.AccountExistsException
import com.example.kiss.exception.AccountNotFoundException


/**
 * It is Q in CQRS
 * Query storage
 * */
class AccountRepositoryInMemoryImpl : AccountRepository {

    private val db: MutableMap<String, AccountEntity> = mutableMapOf()

    override fun createAccount(id: String){
        synchronized(this) {
            if(db.containsKey(id))
                throw AccountExistsException()
            db.put(id, AccountEntity(id, 0.0))
        }
    }

    override fun checkAccountExists(id: String): Boolean {
        return db.contains(id)
    }

    override fun getAccountBalance(id: String): Double {
        return db[id]?.balance ?: throw AccountNotFoundException("AccountNotFoundException Id=$id")
    }

    override fun addAccountAmount(id: String, amount: Double) {
        synchronized(this) {
            if (db.containsKey(id)) {
                db[id]!!.balance += amount
            }
        }
    }

    override fun removeAccountAmount(id: String, amount: Double) {
        synchronized(this) {
            if (db.containsKey(id)) {
                db[id]!!.balance -= amount
            }
        }
    }
}
