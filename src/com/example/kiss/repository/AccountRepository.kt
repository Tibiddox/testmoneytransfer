package com.example.kiss.repository


/**
 * Like read db interface for query in CQRS
 * */
interface AccountRepository {
    fun getAccountBalance(id: String): Double
    fun addAccountAmount(id: String, amount: Double)
    fun removeAccountAmount(id: String, amount: Double)
    fun createAccount(id: String)
    fun checkAccountExists(id: String): Boolean
}