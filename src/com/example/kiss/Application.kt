package com.example.kiss


import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.gson.*
import io.ktor.features.*
import io.ktor.request.receive
import com.example.kiss.aggregate.AggregateFactory
import com.example.kiss.aggregate.account.AccountAggregate
import com.example.kiss.aggregate.account.AccountAggregateFactoryInMemoryImpl
import com.example.kiss.domain.AddMoneyForAccountCommand
import com.example.kiss.domain.RemoveMoneyForAccountCommand
import com.example.kiss.domain.transfer.TransferMoneyForAnotherAccountCommand
import com.example.kiss.dto.*
import com.example.kiss.messagebus.handler.EventHandler
import com.example.kiss.messagebus.handler.EventHandlerInMemoryImpl
import com.example.kiss.eventstore.EventStore
import com.example.kiss.eventstore.EventStoreInMemoryImpl
import com.example.kiss.messagebus.MessageBus
import com.example.kiss.messagebus.MessageBusInMemoryImpl
import com.example.kiss.messagebus.MessageSubAggregate
import com.example.kiss.messagebus.MessageSubAggregateImpl
import com.example.kiss.repository.AccountRepository
import com.example.kiss.repository.AccountRepositoryInMemoryImpl
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.singleton
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

fun main(args: Array<String>): Unit{
    embeddedServer(Netty, port = 8080) {
        module()
    }.start(wait = true)
}

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = true) {
    install(ContentNegotiation) {
        gson {
        }
    }

    val kodein = Kodein {
        bind<EventHandler>() with singleton { EventHandlerInMemoryImpl(instance(), instance(), instance()) }
        bind<AccountRepository>() with singleton { AccountRepositoryInMemoryImpl() }
        bind<MessageBus>() with singleton { MessageBusInMemoryImpl() }
        bind<EventStore>() with singleton { EventStoreInMemoryImpl() }
        bind<AggregateFactory<AccountAggregate>>() with singleton { AccountAggregateFactoryInMemoryImpl(
            instance(), instance(), instance(), instance()) }
        bind<MessageSubAggregate>() with singleton { MessageSubAggregateImpl(instance(), instance()) }
    }

    routing {
        /**
         * Create the account. Create aggregate for account over factory
         * Reply the name of account
         * */
        post("/account/create"){
            val body = call.receive<CreateAccountDto>()
            /**
             * first simple check in read db. Just for not produce so many commands/events, and don't make
             * so many load on command db
             * */
            val repository: AccountRepository = kodein.instance()
            if(repository.checkAccountExists(body.name)) {
                call.respond(BaseResponseDto(error = "Account already exists"))
                return@post
            }

            val factory: AggregateFactory<AccountAggregate> = kodein.instance()
            factory.createAggregate(body.name)
            call.respond(BaseResponseDto(data = body.name))
        }

        /**
         * Add money to account.
         * Reply that command got
         * */
        post("/account/add"){
            val body = call.receive<AddMoneyToAccountDto>()

            kodein.instance<MessageBus>().publishMessage(AddMoneyForAccountCommand(
                amount = body.amount,id = body.name
            ))
            call.respond(BaseResponseDto(data = "Request received"))
        }

        /**
         * Remove money from account
         * Reply that command got
         * */
        post("/account/remove"){
            val body = call.receive<RemoveMoneyFromAccountDto>()
            kodein.instance<MessageBus>().publishMessage(RemoveMoneyForAccountCommand(
                amount = body.amount,id = body.name
            ))
            call.respond(BaseResponseDto(data = "Request received"))
        }

        /**
         * Transfer money from one account to another
         * */
        post("/account/transfer"){
            val body = call.receive<TransferMoney>()
            kodein.instance<MessageBus>().publishMessage(TransferMoneyForAnotherAccountCommand(
                amount = body.amount,
                id = body.fromAccount,
                toId = body.toAccount
            ))
            call.respond(BaseResponseDto(data = "Request received"))
        }

        /**
         * Show the balance of account
         * */
        get("/account/{name}/balance") {
            try{
                val balance = kodein.instance<AccountRepository>().getAccountBalance(call.parameters["name"]
                    ?: throw IllegalStateException())
                call.respond(BaseResponseDto(data=balance))
            } catch (ex: Exception) {
                call.respond(BaseResponseDto(error = "Something wrong: ${ex.localizedMessage}"))
            }
        }

        get("/") {
            call.respondText("HELLO WORLD!", contentType = ContentType.Text.Plain)
        }
    }
}

