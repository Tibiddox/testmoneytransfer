package com.example.kiss.dto

data class TransferMoney (
    val fromAccount: String,
    val amount: Double,
    val toAccount: String
)