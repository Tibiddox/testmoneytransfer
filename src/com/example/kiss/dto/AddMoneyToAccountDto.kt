package com.example.kiss.dto

data class AddMoneyToAccountDto(
    val name: String,
    val amount: Double
)