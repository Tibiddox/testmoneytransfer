package com.example.kiss.dto

data class BaseResponseDto(
    val data: Any? = null,
    val error: Any? = null
)