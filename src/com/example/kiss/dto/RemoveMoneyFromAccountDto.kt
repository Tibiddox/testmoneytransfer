package com.example.kiss.dto

data class RemoveMoneyFromAccountDto (
    val name: String,
    val amount: Double
)