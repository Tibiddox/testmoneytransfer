package com.example.kiss.dto

data class CreateAccountDto (
    val name: String
)