package com.example.kiss.aggregate


interface AggregateFactory<T: RootAggregate> {
    fun createAggregate(id: String): T
}