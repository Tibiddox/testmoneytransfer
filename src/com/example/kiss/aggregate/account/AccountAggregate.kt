package com.example.kiss.aggregate.account

import com.example.kiss.aggregate.RootAggregate
import com.example.kiss.domain.*
import com.example.kiss.domain.transfer.*
import com.example.kiss.messagebus.handler.EventHandler
import com.example.kiss.exception.NotEnoughBalanceException
import com.example.kiss.exception.StateLockedException
import com.example.kiss.messagebus.handler.CommandHandler
import com.example.kiss.messagebus.MessageBus

class AccountAggregate constructor(
    id: String,
    val messageBus: MessageBus
): RootAggregate(id), CommandHandler, EventHandler {

    private var balance: Double = 0.0

    private var lockForTransfer = false


    /**
     * Add money
     * */
    override fun commandHandler(command: AddMoneyForAccountCommand) {
        if(lockForTransfer)
            throw StateLockedException("State locked for account $id while $command")
        messageBus.publishMessage(AddMoneyForAccountEvent(id, command.amount))
    }

    override fun eventHandler(event: AddMoneyForAccountEvent) {
        balance += event.amount
    }


    /**
     * Remove money
     * */
    override fun commandHandler(command: RemoveMoneyForAccountCommand) {
        // check state
        if(balance - command.amount < 0.0)
            throw NotEnoughBalanceException("Not enough money for account $id during $command")
        if(lockForTransfer)
            throw StateLockedException("State locked for account $id during $command")

        messageBus.publishMessage(RemoveMoneyForAccountEvent(id, command.amount))
    }

    override fun eventHandler(event: RemoveMoneyForAccountEvent) {
        balance -= event.amount
    }


    /**
     * Transaction for transfer
     * */
    override fun commandHandler(command: TransferMoneyForAnotherAccountCommand) {
        if(balance - command.amount < 0.0)
            throw NotEnoughBalanceException("Not enough money for account $id during $command")
        if(lockForTransfer)
            throw StateLockedException("State locked for account $id during $command")
        messageBus.publishMessage(TransferMoneyForAnotherAccountEvent(
            id = command.id,
            transferId = command.transferId,
            toId = command.toId,
            time = command.time,
            amount = command.amount
        ))
    }

    override fun eventHandler(event: TransferMoneyForAnotherAccountEvent) {
        lockForTransfer = true
    }

    /**
     * Removing complete. Add money to another account
     * */
    override fun commandHandler(command: TransferRemoveMoneyForAccountCommand) {
        if(balance - command.amount < 0.0)
            throw NotEnoughBalanceException("Not enough money for account $id during $command")
        messageBus.publishMessage(TransferRemoveMoneyForAccountEvent(
            id = command.id,
            transferId = command.transferId,
            toId = command.toId,
            time = command.time,
            amount = command.amount
        ))
    }

    override fun eventHandler(event: TransferRemoveMoneyForAccountEvent) {
        balance -= event.amount
    }

    /**
     * Finish transaction for both aggregate
     * */
    override fun commandHandler(command: TransferAddMoneyForAccountCommand) {
        messageBus.publishMessage(TransferAddMoneyForAccountEvent(
            id = command.id,
            transferId = command.transferId,
            fromId = command.fromId,
            time = command.time,
            amount = command.amount
        ))
    }

    override fun eventHandler(event: TransferAddMoneyForAccountEvent) {
        balance += event.amount
    }

    /**
     * Finish transaction. Unlock it
     * */
    override fun commandHandler(command: FinishTransferMoneyToAnotherAccountCommand) {
        messageBus.publishMessage(FinishTransferMoneyToAnotherAccountEvent(
            id = command.id,
            transferId = command.transferId,
            amount = command.amount,
            fromId = command.fromId
        ))
    }

    override fun eventHandler(event: FinishTransferMoneyToAnotherAccountEvent) {
        lockForTransfer = false
    }
}