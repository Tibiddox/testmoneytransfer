package com.example.kiss.aggregate.account

import com.example.kiss.aggregate.AggregateFactory
import com.example.kiss.domain.AddMoneyForAccountEvent
import com.example.kiss.domain.RemoveMoneyForAccountEvent
import com.example.kiss.eventstore.EventStore
import com.example.kiss.messagebus.MessageBus
import com.example.kiss.messagebus.MessageSubAggregate
import com.example.kiss.repository.AccountRepository


class AccountAggregateFactoryInMemoryImpl constructor(
    val eventStore: EventStore,
    val messageBus: MessageBus,
    val messageSubAggregate: MessageSubAggregate,
    val accountRepository: AccountRepository
): AggregateFactory<AccountAggregate> {

    override fun createAggregate(id: String): AccountAggregate {
        synchronized(this) {
            val eventList = eventStore.getEventsForAggregate(id)
            return if(eventList.isEmpty()) {
                accountRepository.createAccount(id)
                val aggregate = AccountAggregate(id, messageBus)
                messageSubAggregate.addCommandHandler(id, aggregate)
                messageSubAggregate.addEventHandler(id, aggregate)
                aggregate
            } else {
                // execute every event on aggregate
                val aggregate = AccountAggregate(id, messageBus)
                for (event in eventList){
                    when (event.event){
                        is AddMoneyForAccountEvent -> aggregate.eventHandler(event.event)
                        is RemoveMoneyForAccountEvent -> aggregate.eventHandler(event.event)
                    }
                }

                aggregate
            }
        }
    }
}