package com.example.kiss.messagebus.handler

import com.example.kiss.domain.*
import com.example.kiss.domain.transfer.*
import com.example.kiss.eventstore.EventStore
import com.example.kiss.eventstore.entity.EventEntity
import com.example.kiss.messagebus.MessageBus
import com.example.kiss.repository.AccountRepository

class EventHandlerInMemoryImpl constructor(
    val eventStore: EventStore,
    val accountRepository: AccountRepository,
    val messageBus: MessageBus
): EventHandler {

    override fun eventHandler(event: AddMoneyForAccountEvent) {
        eventStore.addNewEventForAggregate(
            event.id, EventEntity(
                aggregateName = this.javaClass.simpleName,
                aggregateId = event.id,
                event = AddMoneyForAccountEvent(event.id, event.amount)
            )
        )
        accountRepository.addAccountAmount(event.id,event.amount)
    }

    override fun eventHandler(event: RemoveMoneyForAccountEvent) {
        eventStore.addNewEventForAggregate(
            event.id, EventEntity(
                aggregateName = this.javaClass.simpleName,
                aggregateId = event.id,
                event = RemoveMoneyForAccountEvent(event.id, event.amount)
            )
        )
        accountRepository.removeAccountAmount(event.id,event.amount)
    }

    /**
     * Starting transaction. First remove money from account
     * */
    override fun eventHandler(event: TransferMoneyForAnotherAccountEvent) {
        messageBus.publishMessage(TransferRemoveMoneyForAccountCommand(
            id = event.id,
            amount = event.amount,
            time = event.time,
            toId = event.toId,
            transferId = event.transferId))
    }

    /**
     * Removing complete. Add money to another account
     * */
    override fun eventHandler(event: TransferRemoveMoneyForAccountEvent) {
        messageBus.publishMessage(TransferAddMoneyForAccountCommand(
            id = event.toId,
            amount = event.amount,
            time = event.time,
            fromId = event.id,
            transferId = event.transferId))
    }

    /**
     * Finish transaction for both aggregate
     * */
    override fun eventHandler(event: TransferAddMoneyForAccountEvent) {
        messageBus.publishMessage(FinishTransferMoneyToAnotherAccountCommand(
            id = event.fromId,
            transferId = event.transferId,
            amount = event.amount,
            fromId = event.id))
    }

    /**
     * Store the result in read db
     * */
    override fun eventHandler(event: FinishTransferMoneyToAnotherAccountEvent) {
        accountRepository.removeAccountAmount(event.id, event.amount)
        accountRepository.addAccountAmount(event.fromId, event.amount)
    }
}