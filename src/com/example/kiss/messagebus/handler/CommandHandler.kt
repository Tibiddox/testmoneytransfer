package com.example.kiss.messagebus.handler

import com.example.kiss.domain.AddMoneyForAccountCommand
import com.example.kiss.domain.RemoveMoneyForAccountCommand
import com.example.kiss.domain.transfer.*

interface CommandHandler {
    fun commandHandler(command: AddMoneyForAccountCommand)
    fun commandHandler(command: RemoveMoneyForAccountCommand)
    fun commandHandler(command: TransferMoneyForAnotherAccountCommand)
    fun commandHandler(command: TransferAddMoneyForAccountCommand)
    fun commandHandler(command: TransferRemoveMoneyForAccountCommand)
    fun commandHandler(command: FinishTransferMoneyToAnotherAccountCommand)
}