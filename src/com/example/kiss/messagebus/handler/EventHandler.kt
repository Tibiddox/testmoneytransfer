package com.example.kiss.messagebus.handler

import com.example.kiss.domain.AddMoneyForAccountEvent
import com.example.kiss.domain.RemoveMoneyForAccountEvent
import com.example.kiss.domain.transfer.*

interface EventHandler {
    fun eventHandler(event: AddMoneyForAccountEvent)
    fun eventHandler(event: RemoveMoneyForAccountEvent)
    fun eventHandler(event: TransferMoneyForAnotherAccountEvent)
    fun eventHandler(event: TransferRemoveMoneyForAccountEvent)
    fun eventHandler(event: TransferAddMoneyForAccountEvent)
    fun eventHandler(event: FinishTransferMoneyToAnotherAccountEvent)
}