package com.example.kiss.messagebus

import com.example.kiss.domain.*
import com.example.kiss.domain.transfer.*
import com.example.kiss.messagebus.handler.CommandHandler
import com.example.kiss.messagebus.handler.EventHandler

class MessageSubAggregateImpl  constructor(
    val messageBus: MessageBus,
    val eventHandler: EventHandler
): MessageSubAggregate {

    init {
        inMemoryLoop()
    }

    val commandHandlers = mutableMapOf<String, CommandHandler>()
    val eventHandlers = mutableMapOf<String, EventHandler>()

    /**
     * Something like pub/sub
     * listen the queue, if found something - notify command and event handlers about the message
     * */
    private fun inMemoryLoop() {
        Thread {
            loop@ while (true) {
                if(!messageBus.isEmpty()) {
                    try {
                        val message = messageBus.fetchNextMessage()
                        if (message is Event) {
                            when (message) {
                                is AddMoneyForAccountEvent -> {
                                    eventHandlers[message.id]?.eventHandler(message)
                                        ?: throw IllegalStateException("eventHandler has not init for event $message")
                                    eventHandler.eventHandler(message)
                                }
                                is RemoveMoneyForAccountEvent -> {
                                    eventHandlers[message.id]?.eventHandler(message)
                                        ?: throw IllegalStateException("eventHandler has not init for event $message")
                                    eventHandler.eventHandler(message)
                                }
                                is TransferMoneyForAnotherAccountEvent -> {
                                    eventHandlers[message.id]?.eventHandler(message)
                                        ?: throw IllegalStateException("eventHandler has not init for event $message")
                                    eventHandler.eventHandler(message)
                                }
                                is TransferRemoveMoneyForAccountEvent -> {
                                    eventHandlers[message.id]?.eventHandler(message)
                                        ?: throw IllegalStateException("eventHandler has not init for event $message")
                                    eventHandler.eventHandler(message)
                                }
                                is TransferAddMoneyForAccountEvent -> {
                                    eventHandlers[message.id]?.eventHandler(message)
                                        ?: throw IllegalStateException("eventHandler has not init for event $message")
                                    eventHandler.eventHandler(message)
                                }
                                is FinishTransferMoneyToAnotherAccountEvent -> {
                                    eventHandlers[message.id]?.eventHandler(message)
                                        ?: throw IllegalStateException("eventHandler has not init for event $message")
                                    eventHandler.eventHandler(message)
                                }
                            }
                        } else if (message is Command) {
                            when (message) {
                                is AddMoneyForAccountCommand -> {
                                    commandHandlers[message.id]
                                        ?.commandHandler(message)
                                        ?: throw IllegalStateException("commandHandler has not init for event $message")
                                }
                                is RemoveMoneyForAccountCommand -> {
                                    commandHandlers[message.id]
                                        ?.commandHandler(message)
                                        ?: throw IllegalStateException("commandHandler has not init for event $message")
                                }
                                is TransferMoneyForAnotherAccountCommand -> {
                                    commandHandlers[message.id]
                                        ?.commandHandler(message)
                                        ?: throw IllegalStateException("commandHandler has not init for event $message")
                                }
                                is TransferAddMoneyForAccountCommand -> {
                                    commandHandlers[message.id]
                                        ?.commandHandler(message)
                                        ?: throw IllegalStateException("commandHandler has not init for event $message")
                                }
                                is TransferRemoveMoneyForAccountCommand -> {
                                    commandHandlers[message.id]
                                        ?.commandHandler(message)
                                        ?: throw IllegalStateException("commandHandler has not init for event $message")
                                }
                                is FinishTransferMoneyToAnotherAccountCommand -> {
                                    commandHandlers[message.id]
                                        ?.commandHandler(message)
                                        ?: throw IllegalStateException("commandHandler has not init for event $message")
                                }
                            }
                        }
                    } catch (ex: Exception) {
                        print("Exception ex: ${ex.localizedMessage} \n")
                    }
                }
                Thread.sleep(10)
            }
        }.start()
    }

    override fun addCommandHandler(id: String, commandHandler: CommandHandler) {
        commandHandlers[id] = commandHandler
    }

    override fun addEventHandler(id: String, commandHandler: EventHandler) {
        eventHandlers[id] = commandHandler
    }
}