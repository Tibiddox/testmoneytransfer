package com.example.kiss.messagebus

import com.example.kiss.domain.*
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue

class MessageBusInMemoryImpl: MessageBus {

    private val bus: ConcurrentLinkedQueue<Message> = ConcurrentLinkedQueue()

    /**
     * In production it should be rabbitmq queue.
     * For in memory solution here is while(true) loop that check events every time and store new
     * event to eventstore and in the read db
     * */

    override fun publishMessage(message: Message) {
        bus.add(message)
    }

    override fun isEmpty() = bus.isEmpty()

    override fun fetchNextMessage(): Message? {
        if(bus.isEmpty())
            return null
        return bus.remove()
    }
}