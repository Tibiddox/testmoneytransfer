package com.example.kiss.messagebus

import com.example.kiss.domain.Message

interface MessageBus {
    fun publishMessage(message: Message)

    fun fetchNextMessage(): Message?
    fun isEmpty(): Boolean
}