package com.example.kiss.messagebus

import com.example.kiss.messagebus.handler.CommandHandler
import com.example.kiss.messagebus.handler.EventHandler

interface MessageSubAggregate {
    fun addCommandHandler(id: String, commandHandler: CommandHandler)
    fun addEventHandler(id: String, commandHandler: EventHandler)
}