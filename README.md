*   Kotlin, Ktor, Gson, Kodein, Json API. Hope there are not "heavy frameworks". Ktor, kodein - my first experience
*   In memmory storage. It was a requirement.
*  No logs. 
*  No validation in API level. 
In this task I try to show how I see CQRS, DDD and money transfer (in borders of TEST task).
*  All ids is String. Because it is readable.
*  No revert logic. If something goes wrong during transaction it will stay in non valid state. 
Need a saga implementation or revert logic. But it is too large variants and so many code for TEST task.
*  You can create account, add money, rm money, transfer money from A to B account. You can't set balance less than 0.0
*  In every controller endpoint should be first simple check in read db for state like in /account/create endpoint. 
It is fast and not produce so many commands. Of course it is not safe us from concurrency things, but works in 95%.
For another 5% need to use transaction way (like in transfer).
*  Tests only for API just for show that it works. Need more test coverage but it is time and tasks contains requirement
about api tests.
Tests use Thread sleep for waiting messagebus finish his work
There are THREADS GAMES in the bottom of tests. I tried to testing high load. 
It really depends on how many threads and times for execution waiting and what is the CPU of machine that you use.
I've tested it on 0-100 threads on 8 CPU PC. Time for tests near the 10 sec