package com.example.kiss

import io.ktor.http.*
import kotlin.test.*
import io.ktor.server.testing.*
import java.util.concurrent.atomic.AtomicInteger
import kotlin.random.Random


class ApplicationTest {

    @Test
    fun testRoot() {
        withTestApplication({ module(testing = true) }) {

            /**
             * Looks like in ktor tests you can't map response to model. In off doc they compare strings of response...
             * https://github.com/ktorio/ktor/blob/master/ktor-features/ktor-jackson/jvm/test/io/ktor/tests/jackson/JacksonTest.kt
             * */

            /**
             * Message bus is async. So, for waiting the execution I use Thread.sleep()
             * between requests
             * */

            handleRequest(HttpMethod.Get, "/").apply {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals("HELLO WORLD!", response.content)
            }

            fun checkContentType(type: String) {
                assertEquals(type, "json")
            }

            /**
             * Get balance unexisting account
             * Reposnse = error
             * */
            handleRequest(HttpMethod.Get, "/account/a/balance").apply {
                assertEquals(HttpStatusCode.OK, response.status())
                checkContentType(response.contentType().contentSubtype)
                assertEquals(
                    response.content,
                    "{\"error\":\"Something wrong: AccountNotFoundException Id\\u003da\"}"
                )
            }

            /**
             * Create account a
             * Response = name of account
             * */
            handleRequest(HttpMethod.Post, "/account/create") {
                addHeader("Content-Type", "application/json")
                setBody(
                    """ {
                            "name": "a"
                        }"""
                )
            }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
                checkContentType(response.contentType().contentSubtype)
                assertEquals(response.content, "{\"data\":\"a\"}")
            }

            /**
             * Get balance of created account
             * Response = balance 0.0
             * */
            handleRequest(HttpMethod.Get, "/account/a/balance").apply {
                assertEquals(HttpStatusCode.OK, response.status())
                checkContentType(response.contentType().contentSubtype)
                assertEquals(response.content, "{\"data\":0.0}")
            }

            /**
             * Add 50 to account
             * Response = Request received
             * then balance=50
             * */
            handleRequest(HttpMethod.Post, "/account/add") {
                addHeader("Content-Type", "application/json")
                setBody(
                    """ {
                            "name": "a",
                            "amount": "50"
                        }"""
                )
            }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
                checkContentType(response.contentType().contentSubtype)
                assertEquals(response.content, "{\"data\":\"Request received\"}")
                /**
                 * Get balance of created account
                 * Response = balance 0.0
                 * */
                handleRequest(HttpMethod.Get, "/account/a/balance") {
                    Thread.sleep(500)
                }.apply {
                    assertEquals(HttpStatusCode.OK, response.status())
                    checkContentType(response.contentType().contentSubtype)
                    assertEquals(response.content, "{\"data\":50.0}")
                }
            }

            /**
             * Remove balance from account
             * Response = Request received
             * then balance=45
             * */
            handleRequest(HttpMethod.Post, "/account/remove") {
                addHeader("Content-Type", "application/json")
                setBody(
                    """ {
                            "name": "a",
                            "amount": "5"
                        }"""
                )
            }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
                checkContentType(response.contentType().contentSubtype)
                assertEquals(response.content, "{\"data\":\"Request received\"}")


                /**
                 * Get balance of created account
                 * Response = balance 45.0
                 * */
                handleRequest(HttpMethod.Get, "/account/a/balance") {
                    Thread.sleep(500)
                }.apply {
                    assertEquals(HttpStatusCode.OK, response.status())
                    checkContentType(response.contentType().contentSubtype)
                    assertEquals(response.content, "{\"data\":45.0}")
                }
            }

            /**
             * Remove balance from account
             * Response = Request received
             * then does not execute, balance=45
             * */
            handleRequest(HttpMethod.Post, "/account/remove") {
                addHeader("Content-Type", "application/json")
                setBody(
                    """ {
                            "name": "a",
                            "amount": "500"
                        }"""
                )
            }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
                checkContentType(response.contentType().contentSubtype)
                assertEquals(response.content, "{\"data\":\"Request received\"}")


                /**
                 * Get balance of created account
                 * Response = balance 45.0
                 * */
                handleRequest(HttpMethod.Get, "/account/a/balance") {
                    Thread.sleep(500)
                }.apply {
                    assertEquals(HttpStatusCode.OK, response.status())
                    checkContentType(response.contentType().contentSubtype)
                    assertEquals(response.content, "{\"data\":45.0}")
                }
            }


            /**
             * Create account b
             * Response = name of account
             * */
            handleRequest(HttpMethod.Post, "/account/create") {
                addHeader("Content-Type", "application/json")
                setBody(
                    """ {
                            "name": "b"
                        }"""
                )
            }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
                checkContentType(response.contentType().contentSubtype)
                assertEquals(response.content, "{\"data\":\"b\"}")
            }

            /**
             * Add 100 to account b
             * Response = Request received
             * then balance=45
             * */
            handleRequest(HttpMethod.Post, "/account/add") {
                addHeader("Content-Type", "application/json")
                setBody(
                    """ {
                            "name": "b",
                            "amount": "100"
                        }"""
                )
            }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
                checkContentType(response.contentType().contentSubtype)
                assertEquals(response.content, "{\"data\":\"Request received\"}")
                /**
                 * Get balance of created account
                 * Response = balance 100.0
                 * */
                handleRequest(HttpMethod.Get, "/account/b/balance") {
                    Thread.sleep(500)
                }.apply {
                    assertEquals(HttpStatusCode.OK, response.status())
                    checkContentType(response.contentType().contentSubtype)
                    assertEquals(response.content, "{\"data\":100.0}")
                }
            }

            // a=45, b=100

            /**
             * Transfer 15 from a to b account
             * Response = Request received
             * then a-balance=30, b-balance=115
             * */
            handleRequest(HttpMethod.Post, "/account/transfer") {
                addHeader("Content-Type", "application/json")
                setBody(
                    """{
                            "fromAccount": "a",
                            "amount":"15",
                            "toAccount": "b"
                        }"""
                )
            }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
                checkContentType(response.contentType().contentSubtype)
                assertEquals(response.content, "{\"data\":\"Request received\"}")
                /**
                 * Get balance of created account a
                 * Response = balance 30.0
                 * */
                handleRequest(HttpMethod.Get, "/account/a/balance") {
                    Thread.sleep(1000)
                }.apply {
                    assertEquals(HttpStatusCode.OK, response.status())
                    checkContentType(response.contentType().contentSubtype)
                    assertEquals(response.content, "{\"data\":30.0}")
                }

                /**
                 * Get balance of created account b
                 * Response = balance 30.0
                 * */
                handleRequest(HttpMethod.Get, "/account/b/balance") {
                    Thread.sleep(1000)
                }.apply {
                    assertEquals(HttpStatusCode.OK, response.status())
                    checkContentType(response.contentType().contentSubtype)
                    assertEquals(response.content, "{\"data\":115.0}")
                }
            }

            // a=30, b=115

            /**
             * Transfer 15 from a to b account
             * Response = Request received
             * then does not execute, a-balance=30, b-balance=115
             * */
            handleRequest(HttpMethod.Post, "/account/transfer") {
                addHeader("Content-Type", "application/json")
                setBody(
                    """{
                            "fromAccount": "a",
                            "amount":"150",
                            "toAccount": "b"
                        }"""
                )
            }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
                checkContentType(response.contentType().contentSubtype)
                assertEquals(response.content, "{\"data\":\"Request received\"}")
                /**
                 * Get balance of created account a
                 * Response = balance 30.0
                 * */
                handleRequest(HttpMethod.Get, "/account/a/balance") {
                    Thread.sleep(1000)
                }.apply {
                    assertEquals(HttpStatusCode.OK, response.status())
                    checkContentType(response.contentType().contentSubtype)
                    assertEquals(response.content, "{\"data\":30.0}")
                }

                /**
                 * Get balance of created account b
                 * Response = balance 30.0
                 * */
                handleRequest(HttpMethod.Get, "/account/b/balance") {
                    Thread.sleep(1000)
                }.apply {
                    assertEquals(HttpStatusCode.OK, response.status())
                    checkContentType(response.contentType().contentSubtype)
                    assertEquals(response.content, "{\"data\":115.0}")
                }
            }

            fun addMoney(account: String, amount: Double) {
                handleRequest(HttpMethod.Post, "/account/add") {
                    addHeader("Content-Type", "application/json")
                    setBody(
                        """ {
                            "name": "$account",
                            "amount": "$amount"
                        }"""
                    )
                }.apply {
                    assertEquals(HttpStatusCode.OK, response.status())
                    checkContentType(response.contentType().contentSubtype)
                    assertEquals(response.content, "{\"data\":\"Request received\"}")
                }
            }

            fun rmMoney(account: String, amount: Double) {
                handleRequest(HttpMethod.Post, "/account/remove") {
                    addHeader("Content-Type", "application/json")
                    setBody(
                        """ {
                            "name": "$account",
                            "amount": "$amount"
                        }"""
                    )
                }.apply {
                    assertEquals(HttpStatusCode.OK, response.status())
                    checkContentType(response.contentType().contentSubtype)
                    assertEquals(response.content, "{\"data\":\"Request received\"}")
                }
            }

            fun transferMoney(from:String, to: String, amount: Double) {
                handleRequest(HttpMethod.Post, "/account/transfer") {
                    addHeader("Content-Type", "application/json")
                    setBody(
                        """{
                            "fromAccount": "$from",
                            "amount":"$amount",
                            "toAccount": "$to"
                        }"""
                    )
                }.apply {
                    assertEquals(HttpStatusCode.OK, response.status())
                    checkContentType(response.contentType().contentSubtype)
                    assertEquals(response.content, "{\"data\":\"Request received\"}")
                }
            }

            /**
             * THREADS GAMES
             */

            addMoney("a", 10000.0)
            addMoney("b", 10000.0)
            Thread.sleep(300)
            var aState = AtomicInteger(10030)
            var bState = AtomicInteger(10115)


            val threadCount = 4
            val cpuCount = 8
            val waitingForExecution = if(threadCount < cpuCount)
                threadCount * 1000L
            else
                (threadCount / cpuCount) * 12 * 1000L

            val list = mutableListOf<Thread>()
            for (i in 0..threadCount) {
                list.add(Thread {
                    try {
                        if (Random.nextBoolean()) {
                            addMoney("a", 10.0)
                            rmMoney("b", 10.0)
                            aState.addAndGet(10)
                            bState.addAndGet(-10)
                        } else {
                            /**
                             * Can't track transfer, because locking (not atomic operation)
                             * */
//                            transferMoney("a", "b", 10.0)
//                            aState.addAndGet(-10)
//                            bState.addAndGet(10)
                            addMoney("b", 10.0)
                            rmMoney("a", 10.0)
                            aState.addAndGet(-10)
                            bState.addAndGet(10)
                        }
                    } catch (ex: Exception) {
                        print("\nException here ${ex.localizedMessage}\n")
                    }
                })
            }
            /**
             * For fast start we starting threads in another loop
             * */
            for (thread in list) {
                thread.start()
            }

            /**
             * waiting for async message bus execute all messages
             */
            Thread.sleep(waitingForExecution)

            handleRequest(HttpMethod.Get, "/account/a/balance") {
            }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
                checkContentType(response.contentType().contentSubtype)
                print("\n\n ")
                print("Resp: ${response.content}")
                print("aState: ${aState.get()}")
                assertEquals(response.content, "{\"data\":${aState.get()}.0}")
            }

            handleRequest(HttpMethod.Get, "/account/b/balance") {
            }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
                checkContentType(response.contentType().contentSubtype)
                print("\n\n ")
                print("Resp: ${response.content} ")
                print("bState: ${bState.get()}\n")
                assertEquals(response.content, "{\"data\":${bState.get()}.0}")
            }
        }
    }
}
